# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |


---

### How to use 
    
    Color Picker:
        A square and a rectangle forms my Color Picker.
        The three-dimension equals r, g, and b.
        Square                      Rectangle
             ------------> color r      -----> useless
            ¦                          ¦
            ¦                          ¦
            ¦                          ¦
            ¦                          ¦
            ¦                          ¦
            v                          v
        color g                      color b
        
        Click anywhere on the color picker.
        The square on the rightmost shows the current color you pick.
    
    Brush:
        Click and paste, you can choose your Brush size through dragging the 
        slide above the button.
    
    Eraser:
        Click and erase, you can choose your Eraser size through dragging the 
        slide above the button.
    
    Rectangle:
        Onmousedown: First corner of a rectangle.
        Onmouseup: The diagonal corner of the first corner.
    Triangle:
        Onmousedown: The top angle of a triangle.
        Onmouseup: The bottom angle of a triangle.
    Circle:
        Onmousedown: The center of a circle.
        Onmouseup: The circumference of a circle.
    Undo:
        Click.
    Redo:
        Click.
    Refresh:
        Click.
    Download:
        Click.
    Upload:
        Click and choose a image you want to upload.
    Text:
        Click the button and click on the canvas where you want to put text on.
        A input box will appear and input your text in it.
        When you press 'Enter', the text will draw on the canvas.
    

### Function description

    My WebCanvas only consists of basic components and Advanced tools.
    No bonus function in my WebCanvas.

### Gitlab page link

"https://108062118.gitlab.io/AS_01_WebCanvas/"

### Others (Optional)

    My cursor icon only changes in the Canvas.
    If you just click a button.(ex.brush) and didn't move the cursor to
    the canvas, the cursor won't change.
    

<style>
table th{
    width: 100%;
}
</style>
